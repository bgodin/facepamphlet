/* 
 * File: FacePamphlet.java
 * -----------------------
 * When it is finished, this program will implement a basic social network
 * management system.
 */

import java.awt.event.ActionEvent;
import javax.swing.JButton;
import javax.swing.JLabel;
//import java.io.IOException;
import javax.swing.JTextField;

import acm.graphics.GImage;
import acm.program.*;
import acm.util.ErrorException;

public class FacePamphlet extends Program implements
		FacePamphletConstants {

	private JTextField nameField, statusField, pictureField, friendField;
	private FacePamphletDatabase database;
	private FacePamphletProfile currentProfile;
	private FacePamphletCanvas canvas;

	/**
	 * This method has the responsibility for initializing the interactors in
	 * the application, and taking care of any other initialization that needs
	 * to be performed.
	 */
	public void init() {
		database = new FacePamphletDatabase();
		FacePamphletCanvas canvas = new FacePamphletCanvas();
		this.add( canvas );

		// Add buttons and text fields to display
		this.add( new JLabel( "Name: " ), NORTH );
		nameField = new JTextField( TEXT_FIELD_SIZE );
		add( nameField, NORTH );
		add( new JButton( "Add" ), NORTH );
		add( new JButton( "Delete" ), NORTH );
		add( new JButton( "Lookup" ), NORTH );
		statusField = new JTextField( TEXT_FIELD_SIZE );
		add( statusField, WEST );
		add( new JButton( "Change Status" ), WEST );
		add( new JLabel( EMPTY_LABEL_TEXT ), WEST );
		pictureField = new JTextField( TEXT_FIELD_SIZE );
		add( pictureField, WEST );
		add( new JButton( "Change Picture" ), WEST );
		add( new JLabel( EMPTY_LABEL_TEXT ), WEST );
		friendField = new JTextField( TEXT_FIELD_SIZE );
		add( friendField, WEST );
		add( new JButton( "Add Friend" ), WEST );

		this.addActionListeners();
		nameField.addActionListener( this );
		statusField.addActionListener( this );
		pictureField.addActionListener( this );
		friendField.addActionListener( this );
		
	}

	/**
	 * This class is responsible for detecting when the buttons are clicked or
	 * interactors are used, so you will have to add code to respond to these
	 * actions.
	 */
	public void actionPerformed(ActionEvent e) {
		// top bar
		if (e.getActionCommand().equals("Add")) {
			if (!nameField.getText().isEmpty()) {
				if (database.containsProfile(nameField.getText())) {
					// print out message that profile already exists
					println("profile already exists");
				} else {
					FacePamphletProfile profile = new FacePamphletProfile(nameField.getText());
					database.addProfile(profile);
				}
			}
		}

		if (e.getActionCommand().equals("Delete")) {
			if (!nameField.getText().isEmpty()) {
				// destroy reference to currentView if deleting current user
				if (nameField.getText().toLowerCase().equals(currentProfile.getName().toLowerCase())) {
					currentProfile = null;
				}
				// remove profile from database
				database.deleteProfile(nameField.getText());
				// TODO remove profile from screen
				// TODO display message that profile has been cleared
			}
		}

		if (e.getActionCommand().equals("Lookup")) {
			if (!nameField.getText().isEmpty()) {
				currentProfile = database.getProfile(nameField.getText());
				// test
				if (currentProfile != null) {
					println(currentProfile.toString());
				} else {
					println("profile does not exist");
				}
			}
		}
		// side bars
		if (e.getActionCommand().equals("Change Status") || e.getSource() == statusField) {
			if (!statusField.getText().isEmpty()) {
				if (currentProfile != null) {
					String statusTest = statusField.getText();
					//buggy doesn't check for whether profile exists and doesn't
					//implementation doesn't seem right
					currentProfile.setStatus(statusTest);
					canvas.showMessage(statusTest);
				} else {
					println("please select a profile");
				}
			}
		}
		
		if (e.getActionCommand().equals("Change Picture") || e.getSource() == pictureField) {
			if (!pictureField.getText().isEmpty()) {
				if (currentProfile != null) {
					GImage image = null;
					try {
						image = new GImage(pictureField.getText());
						currentProfile.setImage(image);
					} catch (ErrorException ex) {
						println("Unable to load image file");
					}
				} else {
					println("please select a profile");
				}
			}
		}

		if (e.getActionCommand().equals("Add Friend") || e.getSource() == friendField) {
			if (!friendField.getText().isEmpty()) {
				String friendName = friendField.getText();
				if (currentProfile != null) {
					if (database.containsProfile(friendName)) {
						//check if friend already exists for current user
						for(String s: currentProfile.getFriends()) {
							if (currentProfile.getFriends().contains(s)) {
								println("Friend already added");
								return;
							}
						}
						// else add friend to current user
						currentProfile.addFriend(friendName);
						// also add current user to the new friend's friend list
						database.getProfile(friendName).addFriend(currentProfile.getName());
					} else { // if !database.containsProfile(friendName)
						println("That person doens't exist");
					}
				} else { // if currentView == null
					println("please select a profile");
				}
			}
		}
	}
}
