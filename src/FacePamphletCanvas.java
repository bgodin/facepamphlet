/*
 * File: FacePamphletCanvas.java
 * -----------------------------
 * This class represents the canvas on which the profiles in the social
 * network are displayed.  NOTE: This class does NOT need to update the
 * display when the window is resized.
 */

import acm.graphics.GCanvas;
import acm.graphics.GLabel;

public class FacePamphletCanvas extends GCanvas implements FacePamphletConstants {

		private GLabel showMessage;
		private FacePamphletProfile displayProfile;
		
	   /**
	    * Constructor This method takes care of any initialization needed for
	    * the display
	    */
	   public FacePamphletCanvas() {
		   showMessage = new GLabel("");
		   showMessage.setFont(MESSAGE_FONT);	
		   this.add(showMessage, (APPLICATION_WIDTH - showMessage.getWidth())/2, APPLICATION_HEIGHT - BOTTOM_MESSAGE_MARGIN);
	   }

	   /**
	    * This method displays a message string near the bottom of the canvas.
	    * Every time this method is called, the previously displayed message
	    * (if any) is replaced by the new message text passed in.
	    */
	   public void showMessage( String msg ) {
	//	   if (showMessage.getLabel() != "") {
		//	  this.remove(showMessage);
	//	   }
		   showMessage.setLabel(msg);
	   }

	   /**
	    * This method displays the given profile on the canvas. The canvas is
	    * first cleared of all existing items (including messages displayed
	    * near the bottom of the screen) and then the given profile is
	    * displayed. The profile display includes the name of the user from the
	    * profile, the corresponding image (or an indication that an image does
	    * not exist), the status of the user, and a list of the user's friends
	    * in the social network.
	    */
	   public void displayProfile( FacePamphletProfile profile ) {
		  displayProfile = profile;
		   this.removeAll();
	   }

}
